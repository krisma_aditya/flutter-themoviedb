import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/states/movieapp_state.dart';

void main() {
  // runApp(const MyApp());
  // runApp(TestApp());
  runApp(MovieApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // title: 'Hood App',
      // theme: ThemeData(
      //   // This is the theme of your application.
      //   //
      //   // Try running your application with "flutter run". You'll see the
      //   // application has a blue toolbar. Then, without quitting the app, try
      //   // changing the primarySwatch below to Colors.green and then invoke
      //   // "hot reload" (press "r" in the console where you ran "flutter run",
      //   // or simply save your changes to "hot reload" in a Flutter IDE).
      //   // Notice that the counter didn't reset back to zero; the application
      //   // is not restarted.
      //   primarySwatch: Colors.teal,
      // ),
      // home: const MyHomePage(title: 'Hood App'),

      //latihan 00 : scaffolding
      // home: Scaffold(
      //   // theme: ThemeData(primarySwatch: Colors.blueGrey),
      //   appBar: AppBar(title: Text("Hello World")),
      //   body: Center(child : Container(
      //       color: Colors.teal,
      //       width: 250,
      //       height: 150,
      //       child : Text(
      //         "Aplikasi flutter buatan albertus krisma aditya giovanni",
      //         textAlign: TextAlign.justify,
      //         maxLines: 2,
      //         overflow: TextOverflow.ellipsis,
      //         softWrap: false,
      //         style: TextStyle(color : Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
      //       ))),
      // ),

      //latihan 01 : Column and Row
      // home : Scaffold(
      //   appBar : AppBar(title: Text("Row and Column")),
      //   // Column : vertical ; Row : Horizontal
      //   body: Column(
      //     children: <Widget>[
      //       Text("Albertus"),
      //       Text("Krisma"),
      //       Text("Aditya"),
      //       Text("Giovanni"),
      //       Row(
      //         children: <Widget>[
      //           Text("Albertus"),
      //           Text("Krisma"),
      //           Text("Aditya"),
      //           Text("Giovanni"),
      //         ],
      //       )
      //     ],
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     crossAxisAlignment: CrossAxisAlignment.start,
      //   )
      // )

      // // latihan 02 : container widget
      //   home : Scaffold(
      //       appBar : AppBar(title: Text("Container Widget"), ),
      //       // Column : vertical ; Row : Horizontal
      //       body: Container(
      //         color: Colors.teal,
      //         // padding: EdgeInsets.all(10),
      //         //margin: EdgeInsets.all(10), // untuk menambahkan margin atas kiri bawah kanan 10
      //         child: Container(
      //           // color: Colors.yellow,
      //           margin: EdgeInsets.all(10),
      //           decoration: BoxDecoration(color: Colors.deepOrange, borderRadius: BorderRadius.circular(10)),
      //         )
      //       )
      //   )

      // latihan 03 : Stateless vs Stateful Widget
        home : const MyWidget(title: 'Stateful Widget')
    );
  }
}

class MyWidget extends StatefulWidget {
  const MyWidget({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter ++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter'
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class TestApp extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _TestAppState();
}

class _TestAppState extends State<TestApp>{

  // in this exercise : ListView, setState,

  List<Widget> widgets = [];
  int number = 0;

  _TestAppState(){
    for(int i=0; i < 30; i++){
      widgets.add(
        Container(
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.all(4),
          width: double.infinity,
          height: 40,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(4),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 2,
                    offset: Offset(0, 0)
                )
              ]
          ),

          child: Text("Data " + i.toString(), style: TextStyle(fontSize: 20))),
        );

    }
  }

  void addNumber(){
    setState(() {
      number++;
    });
  }

  void resetNumber(){
    setState(() {
      number = 0;
    });
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      // theme: ThemeData(
      //     primaryColor: Colors.black,
      //   primaryColorBrightness: Brightness.dark
      // ),
      home: Scaffold(

        appBar: AppBar(
          title: Text("Stateful Widget Example"),
          backgroundColor: Colors.black87,
          brightness: Brightness.dark,
        ),


        body: Center(

          // listView example
          // child: ListView(
          //   children: widgets,
          // ),


          child: Column(
            // Column : vertical ; Row : Horizontal
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(10),
                width: double.infinity,
                height: 200,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 0)
                      )
                    ]
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(number.toString(), style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold)),
                  ],
                )

              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Container(
                    margin: EdgeInsets.all(10),
                    child: ElevatedButton(
                      onPressed: addNumber,
                      child: Text("Tambah")
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.all(10),
                    child: ElevatedButton(
                        onPressed: resetNumber,
                        child: Text("Reset")
                    ),
                  ),
                ],
              ),

              // ListView(
              //   children: widgets,
              // ),

              Expanded(
                child: ListView(
                  children: widgets,
                ),
              )
            ],
          )
        ),
      )
    );
  }
  
}

class MovieApp extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => MovieAppState();
}
