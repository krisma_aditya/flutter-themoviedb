

import 'package:flutter/material.dart';
import 'package:untitled/states/movie_details_state.dart';
import 'package:untitled/states/profile_state.dart';

class MovieDetailScreen extends StatefulWidget{

  final int movieId;
  final String movieTitle;

  MovieDetailScreen({Key? key, required this.movieId, required this.movieTitle}) : super(key: key);

  @override
  State<StatefulWidget> createState() => MovieDetailState();

}