
import 'package:shared_preferences/shared_preferences.dart';

class MyPref{
  Future <bool> getLoginStatus() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool("isLoggedIn") ?? false;
  }

  Future <String> getUsername() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString("username") ?? "null";
  }

  Future <String> getSessionId() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString("session_id") ?? "null";
  }

  Future <String> getRequestToken() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString("request_token") ?? "null";
  }
}