import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:untitled/models/account_details.dart';
import 'package:untitled/models/create_session_model.dart';
import 'package:untitled/models/logout_model.dart';
import 'package:untitled/models/movie/account_state_movie_model.dart';
import 'package:untitled/models/movie/favorite_movie_model.dart';
import 'package:untitled/models/movie/mark_as_favorite_model.dart';
import 'package:untitled/models/movie/movie_details_model.dart';
import 'package:untitled/models/movie/now_playing_model.dart';
import 'package:untitled/models/request_token_model.dart';
import 'package:untitled/models/validate_token_model.dart';

class ApiServices{

  static const _api_key = "359adc96ea8d88c5d497eef88aaf3b0c";
  static const _baseUrl = "api.themoviedb.org";
  static const String language_en = "en-US";
  static const String media_type_movie = "movie";
  static const String _GET_ACCOUNT_DETAILS = "/3/account";
  static const String _SESSION = "/3/authentication/session";
  static const String _REQUEST_NEW_TOKEN = "/3/authentication/token/new";
  static const String _VALIDATE_TOKEN = "/3/authentication/token/validate_with_login";
  static const String _CREATE_SESSION = "/3/authentication/session/new";

  //movies
  static const String _GET_NOW_PLAYING = "/3/movie/now_playing";
  static const String _GET_MOVIE_DETAILS = "3/movie";
  static const String _GET_ACCOUNT_STATES_FOR_A_MOVIE = "/account_states";
  static const String _MARK_MOVIE_AS_FAVORITE = "/favorite";
  static const String _GET_FAVORITE_MOVIES = "/movies";

  Future<RequestToken> requestNewToken() async {
    // method 1
    final queryParam = {
      'api_key' : _api_key
    };

    final uri = Uri.https(_baseUrl, _REQUEST_NEW_TOKEN, queryParam);
    final response = await http.get(uri);

    var jsonObject = json.decode(response.body);

    return RequestToken.createPostResult(jsonObject);
  }

  Future<ValidateToken> validateToken(String reqToken, String username, String password) async {
    // method 1
    final queryParam = {
      'api_key' : _api_key
    };

    final bodyRequest = {
      'username' : username,
      'password' : password,
      'request_token' : reqToken
    };

    final uri = Uri.https(_baseUrl, _VALIDATE_TOKEN, queryParam);
    final response = await http.post(uri, body: bodyRequest);

    var jsonObject = json.decode(response.body);

    return ValidateToken.createPostResult(jsonObject);
  }

  Future<CreateSession> createSession(String reqToken) async {
    // method 1
    final queryParam = {
      'api_key' : _api_key
    };

    final bodyRequest = {
      'request_token' : reqToken
    };

    final uri = Uri.https(_baseUrl, _CREATE_SESSION, queryParam);
    final response = await http.post(uri, body: bodyRequest);

    var jsonObject = json.decode(response.body);

    return CreateSession.createPostResult(jsonObject);
  }

  Future<NowPlaying> getNowPlaying() async {
    // method 1
    final queryParam = {
      'api_key' : _api_key
    };

    final uri = Uri.https(_baseUrl, _GET_NOW_PLAYING, queryParam);
    final response = await http.get(uri);

    var jsonObject = json.decode(response.body);

    // return NowPlaying.createPostResult(jsonObject);
    return NowPlaying.fromJson(jsonObject);
  }

  Future<FavoriteMovieModel> getFavoriteMovies(String sessionId) async {
    // method 1
    final queryParam = {
      'api_key' : _api_key,
      'session_id' : sessionId
      // 'language' : language_en,
      // 'sort_by' : 'created_at.asc',
      // 'page' : 1
    };

    final uri = Uri.https(_baseUrl, _GET_ACCOUNT_DETAILS + '/null' + _MARK_MOVIE_AS_FAVORITE + _GET_FAVORITE_MOVIES, queryParam);
    final response = await http.get(uri);

    var jsonObject = json.decode(response.body);

    // print("=========> INI URINYA COKK =======>" + uri.toString());
    // print("=========> INI BODY REQUESTNYA COKK =======>" + bodyRequest.toString());

    // return NowPlaying.createPostResult(jsonObject);
    return FavoriteMovieModel.fromJson(jsonObject);
  }

  Future<MovieDetails> getMovieDetails(int movieId) async {
    // method 1
    final queryParam = {
      'api_key' : _api_key,
      'language' : language_en
    };

    final uri = Uri.https(_baseUrl, _GET_MOVIE_DETAILS + '/' + movieId.toString(), queryParam);
    final response = await http.get(uri);

    var jsonObject = json.decode(response.body);

    // return NowPlaying.createPostResult(jsonObject);
    return MovieDetails.fromJson(jsonObject);
  }

  Future<AccountStateMovie> getAccountStatesForAMovie(String session_id, int movieId) async {
    // method 1
    final queryParam = {
      'api_key' : _api_key,
      'session_id' : session_id
    };

    final uri = Uri.https(_baseUrl, _GET_MOVIE_DETAILS + '/' + movieId.toString() + _GET_ACCOUNT_STATES_FOR_A_MOVIE, queryParam);
    final response = await http.get(uri);

    var jsonObject = json.decode(response.body);
    // print("getAccountStatesForAMovie :" + jsonObject);

    // return NowPlaying.createPostResult(jsonObject);
    print("=========> INI RESPONSENYA COKK ========> " + AccountStateMovie.fromJson(jsonObject).toString());
    return AccountStateMovie.fromJson(jsonObject);
  }

  Future<MarkAsFavorite> markAsFavorite(String sessionId, int movieId, bool favorite) async {
    // method 1
    final queryParam = {
      'api_key' : _api_key,
      'session_id': sessionId
    };

    final headers = {
      'Content-Type' : 'application/json;charset=utf-8'
    };

    final bodyRequest = {
      'media_type': media_type_movie,
      'media_id': movieId,
      'favorite': favorite
    };

    final uri = Uri.https(_baseUrl, _GET_ACCOUNT_DETAILS + '/null' + _MARK_MOVIE_AS_FAVORITE, queryParam);
    // print("=========> INI URINYA COKK =======>" + uri.toString());
    // print("=========> INI BODY REQUESTNYA COKK =======>" + bodyRequest.toString());

    var jsonObject; 
    try{
      final response = await http.post(uri,headers: headers, body: jsonEncode(bodyRequest));
      jsonObject = json.decode(response.body);
      
    }catch(e){
      print("=========> EXCEPTION =======>" + e.toString());
    }
    

    
    print("=========> INI RESPONSENYA COKK ========> " + MarkAsFavorite.fromJson(jsonObject).toString());

    return MarkAsFavorite.fromJson(jsonObject);
  }

  Future<AccountDetails> getAccountDetails(String session_id) async {
    // method 1
    final queryParam = {
      'api_key' : _api_key,
      'session_id': session_id
    };

    final uri = Uri.https(_baseUrl, _GET_ACCOUNT_DETAILS, queryParam);
    final response = await http.get(uri);

    var jsonObject = json.decode(response.body);

    return AccountDetails.fromJson(jsonObject);
  }

  Future<Logout> logout(String session_id) async {
    // method 1
    final queryParam = {
      'api_key' : _api_key
    };

    final bodyRequest = {
      'session_id' : session_id
    };

    final uri = Uri.https(_baseUrl, _SESSION, queryParam);
    final response = await http.delete(uri, body: bodyRequest);

    var jsonObject = json.decode(response.body);

    return Logout.createPostResult(jsonObject);
  }


}