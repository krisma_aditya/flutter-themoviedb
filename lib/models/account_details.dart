import 'dart:convert';

class AccountDetails{
  bool success;
  int status_code;
  String status_message;

  Avatar avatar;
  int id;
  String name;
  bool include_adult;
  String username;

  AccountDetails(
      this.success,
      this.status_code,
      this.status_message,
      this.avatar,
      this.id,
      this.name,
      this.include_adult,
      this.username
  );

  factory AccountDetails.fromJson(Map jsonMap){
    return AccountDetails(
        jsonMap['success'] as bool,
        jsonMap['status_code'] as int,
        jsonMap['status_message'] as String,
        Avatar.fromJson(jsonMap['avatar']),
        jsonMap['id'] as int,
        jsonMap['name'] as String,
        jsonMap['include_adult'] as bool,
        jsonMap['username'] as String
    );
  }

}

class Avatar{
  Gravatar gravatar;
  Tmdb tmdb;

  Avatar(this.gravatar, this.tmdb);

  factory Avatar.fromJson(dynamic json){
    return Avatar(
      Gravatar.fromJson(json['gravatar']),
      Tmdb.fromJson(json['tmdb'])
    );
  }
}

class Gravatar{
  String hash;
  Gravatar(this.hash);

  factory Gravatar.fromJson(dynamic json){
    return Gravatar(
      json['hash'] as String
    );
  }
}

class Tmdb{
  String avatar_path;
  Tmdb(this.avatar_path);

  factory Tmdb.fromJson(dynamic json){
    return Tmdb(
        json['avatar_path'] as String
    );
  }
}