// API key : 359adc96ea8d88c5d497eef88aaf3b0c

// request token : 1e19241de5e69cd825bba8d6b39d2f8425f552c2

// session ID : f60e9717a54684a4f784c58b63ff0475452bcb58

import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

class MovieList{

  bool success;
  String expires_at;
  String request_token;

  MovieList({required this.success, required this.expires_at, required this.request_token});

  factory MovieList.createPostResult(Map<String, dynamic> object){
    return MovieList(
        success: object['success'],
        expires_at: object['expires_at'],
        request_token: object['request_token']
    );
  }

  static Future<MovieList> getMovieList(String api_key) async {
    // method 1
    final queryParam = {
      'api_key' : api_key
    };

    final uri = Uri.https('api.themoviedb.org', '/3/authentication/token/new', queryParam);
    final response = await http.get(uri);

    var jsonObject = json.decode(response.body);

    return MovieList.createPostResult(jsonObject);
  }

}