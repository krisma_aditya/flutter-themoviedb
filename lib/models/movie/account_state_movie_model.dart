import 'package:equatable/equatable.dart';

class AccountStateMovie extends Equatable{

  bool success;
  int status_code;
  String status_message;

  int id;
  bool favorite;
  bool rated;
  bool watchlist;

  AccountStateMovie(
      this.success,
      this.status_code,
      this.status_message,
      this.id,
      this.favorite,
      this.rated,
      this.watchlist
      );

  factory AccountStateMovie.fromJson(Map jsonMap){
    return AccountStateMovie(
      jsonMap['success'] as bool,
      jsonMap['status_code'] as int,
      jsonMap['status_message'] as String,

      jsonMap['id'] as int,
      jsonMap['favorite'] as bool,
      jsonMap['rated'] as bool,
      jsonMap['watchlist'] as bool
    );
  }

  @override
  List<Object?> get props => [id, favorite, rated, watchlist];

}