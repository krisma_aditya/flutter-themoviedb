class MovieDetails{

  bool success;
  int status_code;
  String status_message;

  int id;
  String title;
  String original_title;
  String overview;
  String poster_path;
  String release_date;
  List<Genres> genres;
  double vote_average;
  int vote_count;

  MovieDetails(
      this.success,
      this.status_code,
      this.status_message,
      this.id,
      this.title,
      this.original_title,
      this.overview,
      this.poster_path,
      this.release_date,
      this.genres,
      this.vote_average,
      this.vote_count
      );

  factory MovieDetails.fromJson(Map jsonMap){
    return MovieDetails(
      jsonMap['success'] as bool,
      jsonMap['status_code'] as int,
      jsonMap['status_message'] as String,

      jsonMap['id'] as int,
      jsonMap['title'] as String,
      jsonMap['original_title'] as String,
      jsonMap['overview'] as String,
      jsonMap['poster_path'] as String,
      jsonMap['release_date'] as String,
      (jsonMap['genres'] as List).map((e) => Genres.fromJson(e)).toList(),
      jsonMap['vote_average'] as double,
      jsonMap['vote_count'] as int,
    );
  }

}

class Genres {
  int id;
  String name;

  Genres(
      this.id,
      this.name
      );

  factory Genres.fromJson(Map jsonMap){
    return Genres(
      jsonMap['id'] as int,
      jsonMap['name'] as String
    );
  }
}