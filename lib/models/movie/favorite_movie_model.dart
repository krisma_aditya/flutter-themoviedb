
import 'package:equatable/equatable.dart';

class FavoriteMovieModel extends Equatable{

  int page;
  List<FavoriteMovie> results;

  FavoriteMovieModel(this.page, this.results);

  factory FavoriteMovieModel.fromJson(Map jsonMap){
    return FavoriteMovieModel(
        jsonMap['page'] as int,
        (jsonMap['results'] as List).map((e) => FavoriteMovie.fromJson(e)).toList());
  }

  @override
  List<Object?> get props => [page, results];
}


class FavoriteMovie extends Equatable{

  int id;
  String original_title;
  String poster_path;
  String release_date;
  int vote_count;

  FavoriteMovie(
    this.id,
    this.original_title,
    this.poster_path,
    this.release_date,
    this.vote_count
  );

  FavoriteMovie.fromJson(Map jsonMap) :                                            
    id = jsonMap['id'],
    original_title = jsonMap['original_title'],
    poster_path = jsonMap['poster_path'],
    release_date = jsonMap['release_date'],
    vote_count = jsonMap['vote_count'];

  @override
  List<Object?> get props => [id, original_title, poster_path, release_date, vote_count];
}