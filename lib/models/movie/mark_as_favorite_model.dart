import 'package:equatable/equatable.dart';

class MarkAsFavorite extends Equatable{

  bool success;
  int status_code;
  String status_message;

  MarkAsFavorite(
      this.success,
      this.status_code,
      this.status_message
      );

  factory MarkAsFavorite.fromJson(Map jsonMap){
    return MarkAsFavorite(
      jsonMap['success'] as bool,
      jsonMap['status_code'] as int,
      jsonMap['status_message'] as String
    );
  }

  @override
  List<Object?> get props => [success, status_code, status_message];

}