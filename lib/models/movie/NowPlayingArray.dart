
class NowPlayingArray{

  int id;
  String original_title;
  String poster_path;
  String release_date;
  int vote_count;


  NowPlayingArray(
    this.id,
    this.original_title,
    this.poster_path,
    this.release_date,
      this.vote_count
  );

  NowPlayingArray.fromJson(Map jsonMap) :
      id = jsonMap['id'],
        original_title = jsonMap['original_title'],
        poster_path = jsonMap['poster_path'],
        release_date = jsonMap['release_date'],
  vote_count = jsonMap['vote_count'];

}