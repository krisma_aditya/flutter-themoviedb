import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'NowPlayingArray.dart';

class NowPlaying{

  int page;
  List<NowPlayingArray> results;

  NowPlaying(this.page, this.results);

  // factory NowPlaying.createPostResult(Map<String, dynamic> object){
  //   return NowPlaying(
  //       page: object['page'],
  //       results: object['results']
  //   );
  // }

  factory NowPlaying.fromJson(Map jsonMap){
    return NowPlaying(
        jsonMap['page'] as int,
        (jsonMap['results'] as List).map((e) => NowPlayingArray.fromJson(e)).toList());
  }

  static Future<NowPlaying> getNowPlaying(String api_key, String reqToken) async {
    // method 1
    final queryParam = {
      'api_key' : api_key
    };

    final uri = Uri.https('api.themoviedb.org', '/3/movie/now_playing', queryParam);
    final response = await http.get(uri);

    var jsonObject = json.decode(response.body);

    // return NowPlaying.createPostResult(jsonObject);
    return NowPlaying.fromJson(jsonObject);
  }

}