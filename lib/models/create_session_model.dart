import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

class CreateSession{

  bool success;
  bool? failure;
  int? status_code;
  String? status_message;
  String? session_id;

  CreateSession({
  required this.success,
  this.failure,
    this.status_code,
    this.status_message,
    this.session_id});

  factory CreateSession.createPostResult(Map<String, dynamic> object){
    return CreateSession(
      success: object['success'],
      failure: object['failure'],
      status_code: object['status_code'],
      status_message: object['status_message'],
      session_id: object['session_id']
    );
  }

  static Future<CreateSession> createSession(String api_key, String reqToken) async {
    // method 1
    final queryParam = {
      'api_key' : api_key
    };

    final bodyRequest = {
      'request_token' : reqToken
    };

    final uri = Uri.https('api.themoviedb.org', '/3/authentication/session/new', queryParam);
    final response = await http.post(uri, body: bodyRequest);

    var jsonObject = json.decode(response.body);

    return CreateSession.createPostResult(jsonObject);
  }

}