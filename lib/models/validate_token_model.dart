import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

class ValidateToken{

  bool success;
  int status_code;
  String status_message;

  ValidateToken({required this.success, required this.status_code, required this.status_message});

  factory ValidateToken.createPostResult(Map<String, dynamic> object){
    return ValidateToken(
      success: object['success'],
      status_code: object['status_code'],
      status_message: object['status_message']
    );
  }

  static Future<ValidateToken> validateToken(String api_key, String reqToken, String username, String password) async {
    // method 1
    final queryParam = {
      'api_key' : api_key
    };

    final bodyRequest = {
      'username' : username,
      'password' : password,
      'request_token' : reqToken
    };

    final uri = Uri.https('api.themoviedb.org', '/3/authentication/token/validate_with_login', queryParam);
    final response = await http.post(uri, body: bodyRequest);

    var jsonObject = json.decode(response.body);

    return ValidateToken.createPostResult(jsonObject);
  }

}