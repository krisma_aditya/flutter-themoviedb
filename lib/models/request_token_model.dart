import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

class RequestToken{

  bool success;
  String expires_at;
  String request_token;

  RequestToken({required this.success, required this.expires_at, required this.request_token});

  factory RequestToken.createPostResult(Map<String, dynamic> object){
    return RequestToken(
      success: object['success'],
      expires_at: object['expires_at'],
      request_token: object['request_token']
    );
  }

  static Future<RequestToken> requestNewToken(String api_key) async {
    // method 1
    final queryParam = {
      'api_key' : api_key
    };

    final uri = Uri.https('api.themoviedb.org', '/3/authentication/token/new', queryParam);
    final response = await http.get(uri);

    var jsonObject = json.decode(response.body);

    return RequestToken.createPostResult(jsonObject);
  }

}