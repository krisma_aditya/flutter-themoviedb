import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

class Logout{

  bool success;
  int status_code;
  String status_message;

  Logout({required this.success, required this.status_code, required this.status_message});

  factory Logout.createPostResult(Map<String, dynamic> object){
    return Logout(
        success: object['success'],
        status_code: object['status_code'],
        status_message: object['status_message']
    );
  }

  static Future<Logout> logout(String api_key, String session_id) async {
    // method 1
    final queryParam = {
      'api_key' : api_key
    };

    final bodyRequest = {
      'session_id' : session_id
    };

    final uri = Uri.https('api.themoviedb.org', '/3/authentication/session', queryParam);
    final response = await http.delete(uri, body: bodyRequest);

    var jsonObject = json.decode(response.body);

    return Logout.createPostResult(jsonObject);
  }

}