
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:untitled/MyPref.dart';
import 'package:untitled/screens/profile.dart';
import 'package:untitled/services/ApiServices.dart';

class ProfileState extends State<ProfileScreen>{

  ApiServices mService = ApiServices();
  String avatar_path = "https://www.themoviedb.org/t/p/w150_and_h150_face";
  MyPref myPref = MyPref();
  late String session_id;

  String avatar_file_name = "";
  String fullname = "";
  String username = "";

  @override
  void initState() {
    super.initState();
    _getSessionId();
    // _getAccountDetails();
  }

  void showToast(String msg){
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.SNACKBAR,
        backgroundColor: Colors.black87,
        textColor: Colors.white,
        fontSize: 14
    );
  }

  Future<void> _getSessionId() async{
    myPref.getSessionId().then((strSessId){
      // showToast("getSessionID success! " + strSessId);

      setState(() {
        this.session_id = strSessId;
      });

      _getAccountDetails();

    });
  }

  Future<void> _getAccountDetails() async {
    mService.getAccountDetails(session_id.toString()).then((response){
      try{
        // if(response.success.toString().isEmpty){
        //   showToast("getAccountDetails success! " + response.success.toString() + response.username);

          this.avatar_file_name = response.avatar.tmdb.avatar_path.toString();
          this.fullname = response.name.toString();
          this.username = response.username.toString();

          setState(() {
            // this.avatar_file_name = response.avatar.tmdb.avatar_path.toString();
          });
        // }
      }catch (e){
        showToast(e.toString());
      }

    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          // backgroundColor: Colors.transparent,
          backgroundColor: Colors.black,
          brightness: Brightness.dark,
          elevation: 0,
          // toolbarHeight: 80,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(14)),

          ),

          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (){
              // showToast("Menu");
              Navigator.of(context).pop();
            },
          ),

          title: Text("Profil", style: TextStyle(color: Colors.white)),

          titleSpacing: 2,
        ),

        body:
            Container(
              margin: EdgeInsets.only(top: 20),
              padding: EdgeInsets.all(40),
              // alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                // mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ClipRRect(

                    borderRadius: BorderRadius.circular(100),
                    child: Image(image: NetworkImage(avatar_path + avatar_file_name), width: 80, height: 80),
                  ),
                  Text(username.toString(), textAlign: TextAlign.center),
                  Text(fullname.toString(), textAlign: TextAlign.center),
                ],
              ),
            ),
      )
    );
  }

}