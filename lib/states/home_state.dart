import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:untitled/blocs/get_favorite_movies/get_favorite_bloc.dart';
import 'package:untitled/blocs/get_favorite_movies/get_favorite_event.dart';
import 'package:untitled/blocs/get_favorite_movies/get_favorite_state.dart';
import 'package:untitled/blocs/movie_details_bloc.dart';
import 'package:untitled/blocs/now_playing_bloc.dart';
import 'package:untitled/main.dart';
import 'package:untitled/models/logout_model.dart';
import 'package:untitled/models/movie/NowPlayingArray.dart';
import 'package:untitled/models/movie/favorite_movie_model.dart';
import 'package:untitled/models/movie/now_playing_model.dart';
import 'package:untitled/screens/home.dart';
import 'package:http/http.dart';
import 'package:untitled/MyPref.dart';
import 'package:untitled/screens/login.dart';
import 'package:untitled/screens/movie_details.dart';
import 'package:untitled/screens/profile.dart';
import 'package:untitled/services/ApiServices.dart';

class HomeState extends State<HomeScreen>{

  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  MyPref myPref = MyPref();
  ApiServices mService = ApiServices();
  final nowPlayingBloc = NowPlayingBloc();

  bool? isLoggedIn;
  String? username;
  String? session_id;
  String? request_token;

  List<NowPlayingArray> movieList = [];
  List<Widget> movieListWidget = [];

  Logout? logout = null;

  // HomeState(){
  //   _getLoginStatus();
  // }

  @override
  void initState() {
    super.initState();
    _getLoginStatus();

  }

  @override
  void dispose() {
    super.dispose();
    nowPlayingBloc.dispose();
  }

  void back(){
    Navigator.pop(context);
  }

  void _addEventLoading() async{
    nowPlayingBloc.eventSink.add(NowPlayingEvent.loading);
    print("now playing aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    // showToast("calling event loading now playing");
  }

  Future<void> _getLoginStatus() async{
    myPref.getLoginStatus().then((boolLoggedIn){

      if(!boolLoggedIn || boolLoggedIn == null){
        // showToast("bool isLoggedIn is empty!");
        moveToLoginScreen();
      }else{
        setState(() {
          isLoggedIn = boolLoggedIn;
        });

        myPref.getUsername().then((value){
          setState(() {
            username = value.toString();
          });

          myPref.getSessionId().then((strSessId){
            setState(() {
              session_id = strSessId;
            });

            myPref.getRequestToken().then((strReqTok){
              setState(() {
                request_token = strReqTok;
              });

              // _refreshMovieList0(myProgressDialog("please wait..."));
              // _addEventLoading();
            });

          });
        });
      }
    });
  }

  ProgressDialog myProgressDialog(String msg){
    final ProgressDialog pr = ProgressDialog(context, type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
      message: msg,
      borderRadius: 4,
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10,
      insetAnimCurve: Curves.easeInOut,
      // progress: 0,
      // maxProgress: 100
    );

    return pr;
  }

  // Future<void> _refreshMovieList0(ProgressDialog? progressDialog) async {
  //
  //   progressDialog!.show();
  //
  //   if(request_token != null){
  //     mService.getNowPlaying(request_token.toString()).then((response){
  //       if(response.results.isNotEmpty || response.results.length > 0){
  //         showToast("movie API call get now playing success");
  //
  //         setState(() {
  //           movieList = response.results;
  //         });
  //         progressDialog.hide();
  //       }
  //     });
  //   }
  // }

  void destroySession(ProgressDialog pr){
    pr.show();

    mService.logout(session_id!).then((response){

      if(response.success){
        showToast("logout sukses");
      }else{
        showToast("logout gagal");
      }

      pr.hide();
      if(response.success){
        deleteLoginData();
        moveToLoginScreen();
      }
    });

  }

  void deleteLoginData() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.remove("isLoggedIn");
    pref.remove("username");
    pref.remove("session_id");
    pref.remove("request_token");
    pref.clear();
  }

  void moveToLoginScreen(){
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context){
      return LoginScreen();
    }));
  }

  void moveToProfileScreen(){
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return ProfileScreen();
    }));
  }

  void moveToMovieDetailScreen(int movieId, String movieTitle){
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return MovieDetailScreen(movieId: movieId, movieTitle: movieTitle,);
    }));
  }

  void showToast(String msg){
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.SNACKBAR,
        backgroundColor: Colors.black87,
        textColor: Colors.white,
        fontSize: 14
    );
  }

  Future<void> _showMyDialog(ProgressDialog pr) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Logout"),
            content: Text("Apakah anda yakin ingin logout?"),
            actions: <Widget>[
              ElevatedButton(
                child: Text("Batal"),
                onPressed: () {
                  Navigator.of(context).pop();
                }
              ),

              ElevatedButton(
                child: Text("Ya, logout"),
                onPressed: () {
                  Navigator.of(context).pop();
                  destroySession(pr);
                }
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {

    final ProgressDialog pr = ProgressDialog(context, type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
      message: 'harap tunggu..',
      borderRadius: 4,
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10,
      insetAnimCurve: Curves.easeInOut,
      // progress: 0,
      // maxProgress: 100
    );

    return MaterialApp(
        home: DefaultTabController(
              length: 4,
              child: Builder(
                builder: (BuildContext context){
                  final TabController tabController = DefaultTabController.of(context)!;
                  tabController.addListener(() {
                    if(!tabController.indexIsChanging){
                      int tabNum = tabController.index;
                      // showToast(tabController.index.toString());
                      if(tabNum == 0){
                        // showToast("Anda sekarang berada di Tab Home");
                      }
                      if(tabNum == 1){
                        // showToast("Anda sekarang berada di Tab Now Playing");
                        _addEventLoading();
                      }
                      if(tabNum == 2){
                        // showToast("Anda sekarang berada di Tab Most Popular");
                        // BlocProvider(
                        //   create: (context) => GetFavoriteBloc(session_id: this.session_id!)..add(GetFavoriteEvents.fetchData),
                        //   child: BlocBuilder<GetFavoriteBloc, GetFavoriteState>(builder: (context, GetFavoriteState state){
                        //     if(state is GetFavoriteLoading){
                        //       return Center(child: CircularProgressIndicator());
                        //     }
                        //     else if(state is GetFavoriteDone){
                        //       return myFavoriteMovies(state.favoriteMovieModel.results);
                        //     }

                        //     return Center(child: Text("unknown state"));
                        //   }),
                        // );
                      }
                      if(tabNum == 3){
                        // showToast("Anda sekarang berada di Tab Favorites");
                      }
                    }
                  });

              return Scaffold(
                // extendBodyBehindAppBar: true,
                backgroundColor: Colors.white,

                appBar: AppBar(
                  // backgroundColor: Colors.transparent,
                  backgroundColor: Colors.black,
                  brightness: Brightness.dark,
                  elevation: 0,
                  // toolbarHeight: 80,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.vertical(bottom: Radius.circular(14)),

                  ),
                  // centerTitle: true,

                  // dipakai untuk memberi warna gradasi
                  // flexibleSpace: Container(
                  //   decoration: BoxDecoration(
                  //     gradient: LinearGradient(
                  //       colors: [Colors.purple, Colors.red],
                  //       begin: Alignment.bottomRight,
                  //       end: Alignment.topLeft
                  //     )
                  //   ),
                  // ),

                  leading: IconButton(
                    icon: Icon(Icons.menu),
                    onPressed: (){
                      showToast("Menu");
                    },
                  ),

                  actions: [
                    IconButton(
                      icon: Icon(Icons.person),
                      onPressed: (){
                        moveToProfileScreen();
                      },
                    ),

                    // IconButton(
                    //   icon: Icon(Icons.refresh),
                    //   onPressed: (){
                    //     // _refreshMovieList0(pr);
                    //     _addEventLoading();
                    //   },
                    // ),

                    IconButton(
                      icon: Icon(Icons.logout),
                      onPressed: (){
                        // destroySession(pr);
                        _showMyDialog(pr);
                      },
                    )
                  ],

                  title: Text("Hai, " + this.username.toString(), style: TextStyle(color: Colors.white)),

                  titleSpacing: 2,
                  bottom: TabBar(
                    tabs: [
                      Tab(icon: Icon(Icons.home), text: 'Home'),
                      Tab(icon: Icon(Icons.play_circle_fill), text: 'Now Playing'),
                      Tab(icon: Icon(Icons.favorite), text: 'My Favorite'),
                      Tab(icon: Icon(Icons.thumb_up), text: 'Most Popular'),
                    ],
                  ),
                ),

                body: TabBarView(
                  children: [
                    homeWidget(),
                    StreamBuilder<List<NowPlayingArray>>(
                        stream: nowPlayingBloc.nowPlayingStream,
                        initialData: [],
                        builder: (context, snapshot){
                          if(snapshot.hasError){
                            return Center(
                              child: TextButton(
                                child: Text(snapshot.error.toString()),
                                onPressed: (){
                                  _addEventLoading();
                                },
                              )
                            );
                          }

                          if(snapshot.connectionState == ConnectionState.waiting){
                            return Center(child: CircularProgressIndicator());
                          }

                          if(snapshot.hasData){
                            var data = snapshot.data;
                            return nowPlayingWidgetTwo(data!);
                          }else{
                            return Center(child: Text("GAK ADA DATA"));
                          }
                        }
                    ),
                    // mostPopularWidget(),
                    BlocProvider(
                          create: (context) => GetFavoriteBloc(session_id: this.session_id!)..add(GetFavoriteEvents.fetchData),
                          child: BlocBuilder<GetFavoriteBloc, GetFavoriteState>(builder: (context, GetFavoriteState state){
                            if(state is GetFavoriteLoading){
                              return Center(child: CircularProgressIndicator());
                            }
                            else if(state is GetFavoriteDone){
                              if(state.favoriteMovieModel.results.length > 0){
                                return myFavoriteMovies(state.favoriteMovieModel.results);
                              }else{
                                return Center(child: Text("Anda belum memiliki film favorit"));
                              }
                              
                            }

                            return Center(child: Text("unknown state"));
                          }),
                    ),
                    mostPopularWidget()
                  ],
                )
              );
                })
            ),

    );
  }

  Widget nowPlayingWidget() => Column(
    children: <Widget>[
      Container(
        margin: EdgeInsets.only(top: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("login status      : " + this.isLoggedIn.toString(),style: TextStyle(color: Colors.black87)),
            Text("session_id        : " + this.session_id.toString(),style: TextStyle(color: Colors.black87)),
            Text("request_token     : " + this.request_token.toString(),style: TextStyle(color: Colors.black87)),
          ],
        ),
      ),

      Expanded(
        child: ListView.builder(
          itemCount: movieList == null ? 0 : movieList.length,
          itemBuilder: (BuildContext context, int index){
            return InkWell(
              onTap: (){
                showToast(movieList.elementAt(index).original_title.toString());
              },
              child: Container(
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(4),
                  width: double.infinity,
                  height: 140,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(4),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 1,
                            blurRadius: 2,
                            offset: Offset(0, 0)
                        )
                      ]
                  ),

                  child: Row(
                    children: <Widget>[
                      // ClipRRect(
                      //   borderRadius: BorderRadius.circular(100),
                      //   child: Image(image: NetworkImage("https://image.tmdb.org/t/p/w200/" + movieList.elementAt(index).poster_path)),
                      // ),
                      Container(
                        child: Image(image: NetworkImage("https://image.tmdb.org/t/p/w200/" + movieList.elementAt(index).poster_path)),
                      ),

                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(movieList.elementAt(index).original_title, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                          Text("Release date : " + movieList.elementAt(index).release_date, style: TextStyle(fontSize: 14)),
                          Text("Likes : " + movieList.elementAt(index).vote_count.toString(), style: TextStyle(fontSize: 14)),
                        ],
                      )

                    ],
                  )
              )
            );

          },
        ),
      )


    ],
  );

  Widget nowPlayingWidgetTwo(List<NowPlayingArray> data) => Column(
    children: <Widget>[
      Container(
        margin: EdgeInsets.only(top: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("Now Playing Movies",style: TextStyle(color: Colors.black87))
          ],
        ),
      ),

      Expanded(
        child: ListView.builder(
          itemCount: data == null ? 0 : data.length,
          itemBuilder: (BuildContext context, int index){
            return InkWell(
                onTap: (){
                  // showToast(data.elementAt(index).original_title.toString());
                  moveToMovieDetailScreen(data.elementAt(index).id, data.elementAt(index).original_title);
                },
                child: Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(4),
                    width: double.infinity,
                    height: 140,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(4),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 1,
                              blurRadius: 2,
                              offset: Offset(0, 0)
                          )
                        ]
                    ),

                    child: Row(
                      children: <Widget>[
                        // ClipRRect(
                        //   borderRadius: BorderRadius.circular(100),
                        //   child: Image(image: NetworkImage("https://image.tmdb.org/t/p/w200/" + movieList.elementAt(index).poster_path)),
                        // ),
                        Container(
                          child: Image(image: NetworkImage("https://image.tmdb.org/t/p/w200/" + data.elementAt(index).poster_path)),
                        ),

                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(data.elementAt(index).original_title, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                              Text("Release date : " + data.elementAt(index).release_date, style: TextStyle(fontSize: 14)),
                              Text("Likes : " + data.elementAt(index).vote_count.toString(), style: TextStyle(fontSize: 14)),
                            ],
                          )
                        ),
                      ],
                    )
                )
            );
          },
        ),
      )
    ],
  );

  Widget myFavoriteMovies(List<FavoriteMovie> data) => Column(
    children: <Widget>[
      Container(
        margin: EdgeInsets.only(top: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("My Favorite Movies",style: TextStyle(color: Colors.black87))
          ],
        ),
      ),

      Expanded(
        child: ListView.builder(
          itemCount: data == null ? 0 : data.length,
          itemBuilder: (BuildContext context, int index){
            return InkWell(
                onTap: (){
                  // showToast(data.elementAt(index).original_title.toString());
                  moveToMovieDetailScreen(data.elementAt(index).id, data.elementAt(index).original_title);
                },
                child: Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(4),
                    width: double.infinity,
                    height: 140,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(4),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 1,
                              blurRadius: 2,
                              offset: Offset(0, 0)
                          )
                        ]
                    ),

                    child: Row(
                      children: <Widget>[
                        // ClipRRect(
                        //   borderRadius: BorderRadius.circular(100),
                        //   child: Image(image: NetworkImage("https://image.tmdb.org/t/p/w200/" + movieList.elementAt(index).poster_path)),
                        // ),
                        Container(
                          child: Image(image: NetworkImage("https://image.tmdb.org/t/p/w200/" + data.elementAt(index).poster_path)),
                        ),

                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(data.elementAt(index).original_title, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                              Text("Release date : " + data.elementAt(index).release_date, style: TextStyle(fontSize: 14)),
                              Text("Likes : " + data.elementAt(index).vote_count.toString(), style: TextStyle(fontSize: 14)),
                            ],
                          )
                        ),
                      ],
                    )
                )
            );
          },
        ),
      )
    ],
  );


  Widget homeWidget() => Column(
    children: <Widget>[
      Container(
        margin: EdgeInsets.only(top: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("login status      : " + this.isLoggedIn.toString(),style: TextStyle(color: Colors.black87)),
            Text("session_id        : " + this.session_id.toString(),style: TextStyle(color: Colors.black87)),
            Text("request_token     : " + this.request_token.toString(),style: TextStyle(color: Colors.black87)),
          ],
        ),
      ),

      Container(
        child: Center(
          child: Text("HOME"),
        )
      )
    ],
  );

  Widget mostPopularWidget() => Column(
    children: <Widget>[
      Container(
        margin: EdgeInsets.only(top: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("Most Popular",style: TextStyle(color: Colors.black87)),
          ],
        ),
      ),

      Container(
          child: Center(
            child: Text("Coming soon, in progress"),
          )
      )
    ],
  );

}