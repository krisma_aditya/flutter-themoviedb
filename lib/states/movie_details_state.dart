import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:untitled/MyPref.dart';
import 'package:untitled/blocs/account_state_movies/account_movie_bloc.dart';
import 'package:untitled/blocs/account_state_movies/account_movie_event.dart';
import 'package:untitled/blocs/account_state_movies/account_movie_state.dart';
import 'package:untitled/blocs/mark_as_favorite/mark_favorite_bloc.dart';
import 'package:untitled/blocs/mark_as_favorite/mark_favorite_event.dart';
import 'package:untitled/blocs/mark_as_favorite/mark_favorite_state.dart';
import 'package:untitled/blocs/movie_details_bloc.dart';
import 'package:untitled/models/movie/movie_details_model.dart';
import 'package:untitled/screens/movie_details.dart';

class MovieDetailState extends State<MovieDetailScreen>{

  String movie_poster_path_w200 = "https://image.tmdb.org/t/p/w200/";
  String movie_poster_path_w300 = "https://image.tmdb.org/t/p/w300/";
  String movie_poster_path_w400 = "https://image.tmdb.org/t/p/w400/";
  String movie_poster_path_w500 = "https://image.tmdb.org/t/p/w500/";

  // int movieId;
  late MovieDetailsBloc movieDetailsBloc;
  late AccountMovieBloc accountMovieBloc;
  late String session_id;
  MyPref myPref = MyPref();

  // MovieDetailState({Key? key, required this.movieId});



  void getSessionId(){
    myPref.getSessionId().then((strSessId){
      setState(() {
        session_id = strSessId;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getSessionId();
    movieDetailsBloc = MovieDetailsBloc(widget.movieId);
    // accountMovieBloc = AccountMovieBloc(session_id: null, movieId: widget.movieId);
    _addEventLoading();
  }

  @override
  void dispose() {
    super.dispose();
    movieDetailsBloc.dispose();
  }

  void _addEventLoading() async{
    movieDetailsBloc.eventSink.add(MovieDetailsEvent.loading);
    print("movie details aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    // showToast("calling event loading now playing");
    // _loadAccountMovie();
  }

  void _loadAccountMovie() async{
    accountMovieBloc.add(AccountMovieEvents.fetchData);
  }

  void showToast(String msg){
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.SNACKBAR,
        backgroundColor: Colors.black87,
        textColor: Colors.white,
        fontSize: 14
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            brightness: Brightness.dark,
            elevation: 0,
            // toolbarHeight: 80,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(14)),
            ),

            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: (){
                Navigator.of(context).pop();
              },
            ),

            title: Text(widget.movieTitle.toString(), style: TextStyle(color: Colors.white)),

            titleSpacing: 2,
          ),

          body: SingleChildScrollView(
            // margin: EdgeInsets.only(top: 20),
            padding: EdgeInsets.all(20),
            child: StreamBuilder<MovieDetails>(
                    stream: movieDetailsBloc.movieDetailStream,
                    initialData: null,
                    builder: (context, snapshot){
                      if(snapshot.hasError){
                        return Center(
                            child: TextButton(
                              child: Text(snapshot.error.toString()),
                              onPressed: (){
                                _addEventLoading();
                              },
                            )
                        );
                      }

                      if(snapshot.connectionState == ConnectionState.waiting){
                        return Center(child: CircularProgressIndicator());
                      }

                      if(snapshot.hasData){
                        var data = snapshot.data;
                        // return nowPlayingWidgetTwo(data!);
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: Image(image: NetworkImage(movie_poster_path_w300 + data!.poster_path.toString())),
                            ),

                            // Container(
                            //   width: 200,
                            //   child: Image(image: NetworkImage(movie_poster_path_w400 + data!.poster_path.toString())),
                            // ),

                            Row(
                              children: <Widget>[
                                // BlocProvider(
                                //   create: (context) => AccountMovieBloc(session_id: this.session_id, movieId: widget.movieId)..add(AccountMovieEvents.fetchData),
                                //   child: BlocBuilder<AccountMovieBloc, AccountMovieState>(builder: (BuildContext context, AccountMovieState state){
                                //     if(state is AccountMovieLoading){
                                //       showToast("loading...");
                                //     }
                                //     else if(state is AccountMovieLoaded){
                                //       if(state.accountStateMovie.favorite){
                                //         showToast("This movie is your favorite!");
                                //         return IconButton(
                                //             icon: Icon(Icons.favorite_sharp, size: 28, color: Colors.redAccent),
                                //             onPressed: (){
                                              
                                //             }
                                //         );
                                //       }else{
                                //         showToast("This movie is not your favorite!");
                                //         return IconButton(
                                //             icon: Icon(Icons.favorite_border, size: 28, color: Colors.indigo),
                                //             onPressed: (){

                                //             }
                                //         );
                                //       }
                                //     }else if(state is AccountMovieError){
                                //       showToast("ERROR :" + state.error.toString());
                                //       return IconButton(
                                //           icon: Icon(Icons.favorite_border, size: 28),
                                //           onPressed: (){

                                //           }
                                //       );
                                //     }
                                //     else
                                //       showToast("unknown");
                                //     return IconButton(
                                //         icon: Icon(Icons.favorite_border, size: 28),
                                //         onPressed: (){

                                //         }
                                //     );
                                //   }),
                                // ),

                                // BlocProvider(create: (context) => MarkAsFavoriteBloc(session_id: this.session_id, movieId: widget.movieId, favorite: false)..add(MarkAsFavoriteEvents.markAsFavorite),
                                //                 child: BlocBuilder<MarkAsFavoriteBloc, MarkAsFavoriteState>(builder: (BuildContext context, MarkAsFavoriteState){
                                //                   return BlocProvider.of<AccountMovieBloc>(context)..add(AccountMovieEvents.fetchData);
                                //                 }),
                                //               );

                                // BlocProvider(
                                //   create: (context) => AccountMovieBloc(session_id: this.session_id, movieId: widget.movieId)..add(AccountMovieEvents.fetchData),
                                //   child: BlocBuilder<AccountMovieBloc, AccountMovieState>(builder: (context, AccountMovieState amState){
                                //     // if(state is AccountMovieLoaded){
                                //       return BlocProvider(
                                //         create: (context) => MarkAsFavoriteBloc(session_id: this.session_id, movieId: widget.movieId),
                                //         child: BlocBuilder<MarkAsFavoriteBloc, MarkAsFavoriteState>(builder: (context, MarkAsFavoriteState mafState){

                                //           // BlocProvider.of<AccountMovieBloc>(context)..add(AccountMovieEvents.fetchData);

                                //           if(amState is AccountMovieLoaded){
                                //             if(amState.accountStateMovie.favorite){
                                //               showToast("this is your favorite movie");
                                //               return IconButton(
                                //                 icon: Icon(Icons.favorite_sharp, size: 28, color: Colors.redAccent),
                                //                 onPressed: (){
                                //                   BlocProvider.of<MarkAsFavoriteBloc>(context).add(MarkAsFavoriteEvents.unmarkAsFavorite);


                                //                   if(mafState is MarkAsFavoriteLoading){
                                //                     showToast("UNMARKING this movie as your favorite...");
                                //                   }
                                //                   else if(mafState is MarkAsFavoriteDone){
                                //                     if(mafState.markAsFavorite.status_code == 1){
                                //                       showToast(mafState.markAsFavorite.status_message.toString());
                                //                     }
                                //                   }
                                //                 }
                                //               );
                                //             }else{
                                //               showToast("this is NOT your favorite movie");
                                //               return IconButton(
                                //                 icon: Icon(Icons.favorite_sharp, size: 28, color: Colors.indigo),
                                //                 onPressed: (){
                                //                   BlocProvider.of<MarkAsFavoriteBloc>(context).add(MarkAsFavoriteEvents.markAsFavorite);
                                //                   if(mafState is MarkAsFavoriteLoading){
                                //                     showToast("MARKING this movie as your favorite...");
                                //                   }
                                //                   else if(mafState is MarkAsFavoriteDone){
                                //                     if(mafState.markAsFavorite.status_code == 13){
                                //                       showToast(mafState.markAsFavorite.status_message.toString());
                                //                     }
                                //                   }
                                //                 }
                                //               );
                                //             }
                                //           }

                                //           return IconButton(
                                //             icon: Icon(Icons.favorite_sharp, size: 28, color: Colors.black54),
                                //             onPressed: (){
                                              
                                //             }
                                //           );

                                //       }));
                                //     // }
                                //   }),
                                // ),

                                BlocProvider(
                                  create: (context) => AccountMovieBloc(session_id: this.session_id, movieId: widget.movieId)..add(AccountMovieEvents.fetchData),
                                  child: BlocBuilder<AccountMovieBloc, AccountMovieState>(builder: (context, AccountMovieState amState){
                                    if(amState is AccountMovieLoaded){
                                      if(amState.accountStateMovie.favorite){

                                        showToast("this is your favorite movie!");

                                        return BlocProvider(
                                          create: (context) => MarkAsFavoriteBloc(session_id: this.session_id, movieId: widget.movieId),
                                          child: BlocBuilder<MarkAsFavoriteBloc, MarkAsFavoriteState>(builder: (context, MarkAsFavoriteState mafState){

                                            return IconButton(
                                                icon: Icon(Icons.favorite_sharp, size: 28, color: Colors.redAccent),
                                                onPressed: (){
                                                  BlocProvider.of<MarkAsFavoriteBloc>(context)..add(MarkAsFavoriteEvents.unmarkAsFavorite);


                                                  if(mafState is MarkAsFavoriteLoading){
                                                    showToast("UNMARKING this movie as your favorite...");
                                                  }
                                                  else if(mafState is MarkAsFavoriteDone){
                                                    if(mafState.markAsFavorite.status_code == 1){
                                                      showToast(mafState.markAsFavorite.status_message.toString());
                                                    }
                                                  }
                                                }
                                            );
                                          })
                                        );
                                      }else{
                                        showToast("this is NOT your favorite movie!");

                                        return BlocProvider(
                                          create: (context) => MarkAsFavoriteBloc(session_id: this.session_id, movieId: widget.movieId),
                                          child: BlocBuilder<MarkAsFavoriteBloc, MarkAsFavoriteState>(builder: (context, MarkAsFavoriteState mafState){

                                            return IconButton(
                                                icon: Icon(Icons.favorite_sharp, size: 28, color: Colors.indigo),
                                                onPressed: (){
                                                  BlocProvider.of<MarkAsFavoriteBloc>(context)..add(MarkAsFavoriteEvents.markAsFavorite);
                                                  


                                                  if(mafState is MarkAsFavoriteLoading){
                                                    showToast("MARKING this movie as your favorite...");
                                                  }
                                                  else if(mafState is MarkAsFavoriteDone){
                                                    if(mafState.markAsFavorite.status_code == 13){
                                                      showToast(mafState.markAsFavorite.status_message.toString());
                                                    }
                                                  }
                                                }
                                            );
                                          })
                                        );
                                      }                                        
                                    }

                                    return IconButton(
                                      icon: Icon(Icons.favorite_border, size: 28, color: Colors.black87),
                                      onPressed: (){

                                      }
                                    );
                                    
                                    
                                    // }
                                  }),
                                ),

                                Expanded(
                                  child: Text(data.title.toString(), style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold))
                                )

                              ],
                            ),

                            Text("Release date : " + data.release_date.toString(), style: TextStyle(fontSize: 18)),
                            Text("Rating : " + data.vote_average.toString(), style: TextStyle(fontSize: 18)),
                            Text(
                                "Synopsis : " + "\n" + data.overview.toString(),
                                style: TextStyle(fontSize: 18),
                                textAlign: TextAlign.justify
                            )

                          ],
                        );
                      }else{
                        return Center(child: Text("GAK ADA DATA"));
                      }
                    }
                ),
          ),
        )
    );
  }

}