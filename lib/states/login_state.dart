import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:untitled/main.dart';
import 'package:untitled/models/create_session_model.dart';
import 'package:untitled/models/request_token_model.dart';
import 'package:untitled/models/validate_token_model.dart';
import 'package:untitled/screens/login.dart';
import 'package:untitled/screens/home.dart';
import 'package:http/http.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:untitled/services/ApiServices.dart';


class LoginState extends State<LoginScreen>{

  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  RequestToken? reqTok = null;
  ValidateToken? validateToken = null;
  ApiServices mService = ApiServices();

  String api_key = "359adc96ea8d88c5d497eef88aaf3b0c";

  void showToast(String msg){
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.SNACKBAR,
        backgroundColor: Colors.black87,
        textColor: Colors.white,
        fontSize: 14
    );
  }

  void validateTokenWithLogin(ProgressDialog pr, username, password){
    mService.validateToken(reqTok!.request_token.toString(), username, password).then((value){

      // showToast(
      //     "usrnm : " + username.toString()+
      //     // "\npass : " + password.toString()+
      //         "\n" + value.success.toString()+
      //         " | " + value.status_message.toString());

      setState(() {
        validateToken = value;
      });

      if(value.success){
        mService.createSession(reqTok!.request_token.toString()).then((response){

          showToast("Login success: " + response.success.toString() + " | " + response.session_id.toString());

          if(response.success){
            saveLoginInfo(true, username.toString(), password.toString(), response.session_id.toString(), reqTok!.request_token.toString());
            moveToHomeScreen();
          }
        });
      }else{
        showToast("Login gagal, username atau password anda salah");
      }

      pr.hide();
    });
  }

  void saveLoginInfo(bool isLoggedIn, String username, String password, String session_id, String request_token) async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool("isLoggedIn", isLoggedIn);
    pref.setString("username", username);
    pref.setString("password", password);
    pref.setString("session_id", session_id);
    pref.setString("request_token", request_token);
  }

  void moveToHomeScreen(){
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context){
      return HomeScreen();
    }));
  }

  @override
  Widget build(BuildContext context) {

    final ProgressDialog pr = ProgressDialog(context, type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
      message: 'Proses login, harap tunggu..',
      borderRadius: 4,
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10,
      insetAnimCurve: Curves.easeInOut,
      // progress: 0,
      // maxProgress: 100
    );



    return MaterialApp(
        theme: ThemeData(
            primaryColor: Colors.black,
            primaryColorBrightness: Brightness.dark,
          primarySwatch: Colors.indigo,
          primaryColorDark: Colors.black
        ),
        home: Scaffold(

          // appBar: AppBar(
          //   title: Text("Login TheMovieDb.org"),
          //   backgroundColor: Colors.black87,
          //   brightness: Brightness.dark,
          // ),
          backgroundColor: Colors.white,

          body: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle(
              systemNavigationBarColor: Color(0xFF000000),
              systemNavigationBarDividerColor: null,
              statusBarColor: Colors.black87,
              systemNavigationBarIconBrightness: Brightness.light,
              statusBarIconBrightness: Brightness.light,
              statusBarBrightness: Brightness.light,
            ),

            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 200,
                  height: 200,
                  child: Image(image: NetworkImage("https://pbs.twimg.com/profile_images/1243623122089041920/gVZIvphd.jpg")),
                ),

                Container(
                  child: Center(child : Text("by Krisma Aditya")),
                ),

                Container(
                  child: Center(child: Text("https://gitlab.com/krisma_aditya/flutter-themoviedb"))
                ),

                Container(
                    margin: EdgeInsets.fromLTRB(20,4,20,4),
                    child: TextField(
                      maxLength: 30,
                      onChanged: (value){
                        setState(() {

                        });
                      },
                      controller: usernameController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                          hintText: "username",
                          // icon: Icon(Icons.person),
                          prefixIcon: Icon(Icons.person),
                          prefixStyle: TextStyle(fontWeight: FontWeight.w100),
                          labelText: "Username"
                      ),
                    )
                ),

                Container(
                    margin: EdgeInsets.fromLTRB(20,4,20,4),
                    child: TextField(
                      obscureText: true,
                      controller: passwordController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                          hintText: "password",
                          prefixIcon: Icon(Icons.vpn_key_outlined),
                          labelText: "Password"
                      ),
                    )
                ),

                // Text(
                //     (reqTok != null) ? reqTok!.success.toString() + " | " + reqTok!.request_token.toString() : "No data"
                // ),

                // ElevatedButton(onPressed: requestNewToken, child: Text("LOGIN")),

                Container(
                  margin: EdgeInsets.fromLTRB(20,4,20,4),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          minimumSize: Size(double.infinity, 50)
                      ),
                      onPressed: (){
                        pr.show();

                        if(reqTok != null){
                          validateTokenWithLogin(pr, usernameController.text, passwordController.text);
                        }else{
                          mService.requestNewToken().then((value){

                            // showToast("request token : " + value.request_token + "\nnow validating...");

                            setState(() {
                              reqTok = value;
                            });

                            validateTokenWithLogin(pr, usernameController.text, passwordController.text);
                          });
                        }

                        pr.hide();

                      },
                      child: Text("LOGIN")),
                ),

              ],
            ),
          )


        )
    );
  }
  // @override
  // void debugFillProperties(DiagnosticPropertiesBuilder properties) {
  //   super.debugFillProperties(properties);
  //   properties.add(DiagnosticsProperty<RequestToken>('Null', Null));
  // }

}