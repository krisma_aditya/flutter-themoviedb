import 'dart:async';

import 'package:flutter/material.dart';
import 'package:untitled/models/movie/NowPlayingArray.dart';
import 'package:untitled/models/movie/movie_details_model.dart';
import 'package:untitled/models/movie/now_playing_model.dart';
import 'package:untitled/services/ApiServices.dart';

enum MovieDetailsEvent {loading, loaded, error}

class MovieDetailsBloc{

  static const _api_key = "359adc96ea8d88c5d497eef88aaf3b0c";
  ApiServices mService = ApiServices();
  // int movieId = 0;

  //state controller
  StreamController<MovieDetails> _stateController = StreamController<MovieDetails>.broadcast();
  StreamSink<MovieDetails> get _movieDetailSink => _stateController.sink;
  Stream<MovieDetails> get movieDetailStream => _stateController.stream;

  //event controller
  StreamController<MovieDetailsEvent> _eventController = StreamController<MovieDetailsEvent>();
  //atau juga bisa kaya gini
  final _eventSteamController = StreamController<MovieDetailsEvent>();

  StreamSink<MovieDetailsEvent> get eventSink => _eventController.sink;
  Stream<MovieDetailsEvent> get _eventStream => _eventController.stream;


  // void _mapEventToState(NowPlayingEvent nowPlayingEvent){
  //   if(nowPlayingEvent == NowPlayingEvent.oldList){
  //     // movieList =
  //   }
  // }

  MovieDetailsBloc(int movieId){
    _eventStream.listen((event) async{
      if(event == MovieDetailsEvent.loading){
        try{
          // _nowPlayingSink.add(null);
          var movieDetails = await mService.getMovieDetails(movieId);
          _movieDetailSink.add(movieDetails);
        }catch(e){
          print(e.toString());
          _movieDetailSink.addError('Gagal. Tap untuk mencoba kembali.');
        }
      }
    });
  }

  void dispose(){
    _stateController.close();
    _eventController.close();
  }

}