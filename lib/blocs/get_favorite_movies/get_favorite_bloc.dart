import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/blocs/get_favorite_movies/get_favorite_event.dart';
import 'package:untitled/blocs/get_favorite_movies/get_favorite_state.dart';
import 'package:untitled/services/ApiServices.dart';

class GetFavoriteBloc extends Bloc<GetFavoriteEvents, GetFavoriteState>{
  
  ApiServices mService = ApiServices();
  final String session_id;

  GetFavoriteBloc({
    required this.session_id
  }) : super(GetFavoriteInitState());
  
  
  @override
  Stream<GetFavoriteState> mapEventToState(GetFavoriteEvents event) async* {

    switch(event){
      case GetFavoriteEvents.fetchData:
        yield GetFavoriteLoading();
        var result = await mService.getFavoriteMovies(session_id);
        yield GetFavoriteDone(favoriteMovieModel: result);
        break;
    }
    
  }

}