import 'package:equatable/equatable.dart';
import 'package:untitled/models/movie/favorite_movie_model.dart';

abstract class GetFavoriteState extends Equatable{
  // final
  @override
  List<Object?> get props => [];
}

class GetFavoriteInitState extends GetFavoriteState{}

class GetFavoriteLoading extends GetFavoriteState{}

class GetFavoriteDone extends GetFavoriteState{
  final FavoriteMovieModel favoriteMovieModel;

  GetFavoriteDone({required this.favoriteMovieModel});
}

class GetFavoriteError extends GetFavoriteState{
  final error;
  GetFavoriteError({this.error});
}