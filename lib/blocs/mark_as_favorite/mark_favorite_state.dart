import 'package:equatable/equatable.dart';
import 'package:untitled/models/movie/mark_as_favorite_model.dart';

abstract class MarkAsFavoriteState extends Equatable{
  // final
  @override
  List<Object?> get props => [];
}

class MarkAsFavoriteInitState extends MarkAsFavoriteState{}

class MarkAsFavoriteLoading extends MarkAsFavoriteState{}

class MarkAsFavoriteDone extends MarkAsFavoriteState{
  final MarkAsFavorite markAsFavorite;

  MarkAsFavoriteDone({required this.markAsFavorite});
}

class MarkAsFavoriteError extends MarkAsFavoriteState{
  final error;
  MarkAsFavoriteError({this.error});
}