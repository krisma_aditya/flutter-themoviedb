import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/blocs/mark_as_favorite/mark_favorite_event.dart';
import 'package:untitled/blocs/mark_as_favorite/mark_favorite_state.dart';
import 'package:untitled/services/ApiServices.dart';

class MarkAsFavoriteBloc extends Bloc<MarkAsFavoriteEvents, MarkAsFavoriteState>{
  
  ApiServices mService = ApiServices();
  final String session_id;
  final int movieId;
  // final bool favorite;

  MarkAsFavoriteBloc({
    required this.session_id,
    required this.movieId
    // required this.favorite
  }) : super(MarkAsFavoriteInitState());
  
  
  @override
  Stream<MarkAsFavoriteState> mapEventToState(MarkAsFavoriteEvents event) async* {

    switch(event){
      case MarkAsFavoriteEvents.markAsFavorite:
        yield MarkAsFavoriteLoading();
        var result = await mService.markAsFavorite(session_id, movieId, true);
        yield MarkAsFavoriteDone(markAsFavorite: result);
        break;

      case MarkAsFavoriteEvents.unmarkAsFavorite:
        yield MarkAsFavoriteLoading();
        var result = await mService.markAsFavorite(session_id, movieId, false);
        yield MarkAsFavoriteDone(markAsFavorite: result);
        break;
    }
    
  }

}