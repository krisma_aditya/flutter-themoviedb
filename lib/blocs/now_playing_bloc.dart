import 'dart:async';

import 'package:flutter/material.dart';
import 'package:untitled/models/movie/NowPlayingArray.dart';
import 'package:untitled/models/movie/now_playing_model.dart';
import 'package:untitled/services/ApiServices.dart';

enum NowPlayingEvent {loading, loaded, error}

class NowPlayingBloc{

  static const _api_key = "359adc96ea8d88c5d497eef88aaf3b0c";
  ApiServices mService = ApiServices();

  //state controller
  StreamController<List<NowPlayingArray>> _stateController = StreamController<List<NowPlayingArray>>.broadcast();
  StreamSink<List<NowPlayingArray>> get _nowPlayingSink => _stateController.sink;
  Stream<List<NowPlayingArray>> get nowPlayingStream => _stateController.stream;

  //event controller
  StreamController<NowPlayingEvent> _eventController = StreamController<NowPlayingEvent>();
  //atau juga bisa kaya gini
  final _eventSteamController = StreamController<NowPlayingEvent>();

  StreamSink<NowPlayingEvent> get eventSink => _eventController.sink;
  Stream<NowPlayingEvent> get _eventStream => _eventController.stream;


  // void _mapEventToState(NowPlayingEvent nowPlayingEvent){
  //   if(nowPlayingEvent == NowPlayingEvent.oldList){
  //     // movieList =
  //   }
  // }

  NowPlayingBloc(){
    _eventStream.listen((event) async{
      if(event == NowPlayingEvent.loading){
        try{
          // _nowPlayingSink.add(null);
          var nowPlayingList = await mService.getNowPlaying();
          _nowPlayingSink.add(nowPlayingList.results);
        }catch(e){
         _nowPlayingSink.addError('Gagal. Tap untuk mencoba kembali.');
        }
      }
    });
  }

  void dispose(){
    _stateController.close();
    _eventController.close();
  }

}