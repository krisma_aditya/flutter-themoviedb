import 'package:equatable/equatable.dart';
import 'package:untitled/models/movie/account_state_movie_model.dart';

abstract class AccountMovieState extends Equatable{
  // final
  @override
  List<Object?> get props => [];
}

class AccountMovieInitState extends AccountMovieState{}

class AccountMovieLoading extends AccountMovieState{}

class AccountMovieLoaded extends AccountMovieState{
  final AccountStateMovie accountStateMovie;

  AccountMovieLoaded({required this.accountStateMovie});
}

class AccountMovieError extends AccountMovieState{
  final error;

  AccountMovieError({this.error});
}