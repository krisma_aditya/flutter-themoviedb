import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/blocs/account_state_movies/account_movie_event.dart';
import 'package:untitled/blocs/account_state_movies/account_movie_state.dart';
import 'package:untitled/services/ApiServices.dart';

class AccountMovieBloc extends Bloc<AccountMovieEvents, AccountMovieState>{

  ApiServices mService = ApiServices();
  final String session_id;
  final int movieId;

  AccountMovieBloc({
    required this.session_id,
    required this.movieId
  }) : super(AccountMovieInitState());

  @override
  Stream<AccountMovieState> mapEventToState(AccountMovieEvents event) async*{
    switch(event){
      case AccountMovieEvents.fetchData:
        yield AccountMovieLoading();
        var accountMovieStatus = await mService.getAccountStatesForAMovie(session_id, movieId);
        yield AccountMovieLoaded(accountStateMovie: accountMovieStatus);
        break;
    }
  }

  // void dispose(){
  //   _stateController.close();
  //   _eventController.close();
  // }

}